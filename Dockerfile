FROM registry.lgi.io/libertyglobal/java:8

MAINTAINER bartlomiej.kepa@gmail.com

ENV JBOSS_FUSE_VERSION 6.3.0
ENV JBOSS_FUSE_BUILD 033
ENV CONNECTION_RETRIES 50

RUN curl -XGET -o jboss-fuse-full.zip -L "http://origin-repository.jboss.org/nexus/content/groups/ea/org/jboss/fuse/jboss-fuse-full/${JBOSS_FUSE_VERSION}.redhat-${JBOSS_FUSE_BUILD}/jboss-fuse-full-${JBOSS_FUSE_VERSION}.redhat-${JBOSS_FUSE_BUILD}.zip"
RUN unzip jboss-fuse-full.zip
RUN rm jboss-fuse-full.zip
RUN ln -s  jboss-fuse-${JBOSS_FUSE_VERSION}.redhat-${JBOSS_FUSE_BUILD} jboss-fuse
RUN sed -i 's/#admin=admin/admin=admin/' jboss-fuse/etc/users.properties

ENTRYPOINT jboss-fuse/bin/standalone





